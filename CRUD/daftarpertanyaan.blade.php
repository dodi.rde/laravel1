@extends('forumSanberCode.masterforum')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-10">
            <h3 class="mt-3">Daftar Pertanyaan</h3>
            <a class="btn btn-primary" href="/pertanyaan/create">Masukkan Pertanyaan Baru</a>

            <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">No</th>
                <th scope="col">id</th>
                <th scope="col">Judul</th>
                <th scope="col">isi</th>
                <th scope="col">Ditanyakan oleh</th>
                <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($pertanyaan as $ask)
                <tr>
                <th scope="row">{{ $loop->iteration}}</th>
                <td>{{$ask->id}}</td>
                <td>{{$ask->judul}}</td>
                <td>{{$ask->isi}}</td>
                <td>{{$ask->user_id}}</td>
                <td> 
                    <a href="/pertanyaan/{{$ask->id}}" class="badge badge-primary">show</a>
                    <a href="/pertanyaan/{{$ask->id}}/edit" class="badge badge-success">edit</a>
                    <a input type="submit" value="delete" class="badge badge-danger">hapus</a>
                    <a href="" class="badge badge-info">jawab</a>
                </td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection