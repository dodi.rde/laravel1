@extends('forumSanberCode.masterforum')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-10">
            <h3 class="mt-3">Daftar User</h3>

            <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">No.</th>
                <th scope="col">Nama Lengkap</th>
                <th scope="col">id</th>
                <th scope="col">email</th>
                <th scope="col">Foto</th>
                <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach( $profil as $pro )
                <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$pro->nama_lengkap}}</td>
                <td>{{$pro->id}}</td>
                <td>{{$pro->email}}</td>
                <td>{{$pro->foto}}</td>
                <td>
                    <a href="" class="badge badge-success">edit</a>
                    <a href="" class="badge badge-danger">hapus</a>
                </td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection