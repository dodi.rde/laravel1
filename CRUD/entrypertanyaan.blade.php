@extends('forumSanberCode.masterforum')
@section('content')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Masukkan Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="/pertanyaan/store">
                  @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="InputJudul">Judul</label>
                    <input type="text" class="form-control" id="InputJudul" name="InputJudul" placeholder="Judul Pertanyaan Anda">
                  </div>
                  <div class="form-group">
                    <label for="InputIsi">Isi</label>
                    <input type="text" class="form-control" id="InputIsi" name="InputIsi" placeholder="Isi Pertanyaan Anda">
                  </div>
                  <div class="form-group">
                        <label>User ID</label>
                        <select class="form-control" id="InputID" name="InputID">
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Buat Pertanyaan</button>
                  <a class="btn btn-primary" href="/pertanyaan">Batal</a>
                </div>
              </form>
            </div>
@endsection