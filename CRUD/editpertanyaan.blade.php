@extends('forumSanberCode.masterforum')
@section('content')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Ubah Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="/pertanyaan/{id}">
                  @csrf
                  @method('put')
                <div class="card-body">
                  <div class="form-group">
                    <label for="InputJudul">Judul</label>
                    <input type="text" class="form-control" id="InputJudul" name="InputJudul" value="{{$pertanyaan->judul}}" placeholder="Judul Pertanyaan Anda">
                  </div>
                  <div class="form-group">
                    <label for="InputIsi">Isi</label>
                    <input type="text" class="form-control" id="InputIsi" name="InputIsi" value="{{$pertanyaan->isi}}" placeholder="Isi Pertanyaan Anda">
                  </div>
                  <div class="form-group">
                        <label>User ID</label>
                        <select class="form-control" id="InputID" name="InputID" value="{{$pertanyaan->user_id}}">
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Ubah Pertanyaan</button>
                  <a class="btn btn-primary" href="/pertanyaan">Batal</a>
                </div>
              </form>
            </div>
@endsection